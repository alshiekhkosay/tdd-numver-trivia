import 'package:dartz/dartz.dart';
import 'package:number_trivia/core/errors/failure.dart';

class InputConverter {
  Either<Failure, int> stringToUnSignedInteger(String str) {
    try {
      final number = int.parse(str);
      if (number < 0) {
        throw const FormatException();
      } else {
        return Right(number);
      }
    } on FormatException {
      return Left(InvalidInputFailure());
    }
  }
}

class InvalidInputFailure extends Failure {}
