// ignore_for_file: constant_identifier_names

import 'dart:convert';

import 'package:number_trivia/core/errors/exception.dart';
import 'package:number_trivia/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

const CASHED_NUMBER_TRIVIA = 'CASHED_NUMBER_TRIVIA';

abstract class NumberTriviaLocalDataSource {
  ///Gets the Cashed [NumberTriviaModel] which was cashed
  ///the last time the user has internet connection
  ///
  ///Throws [CacheException] on error code
  Future<NumberTriviaModel> getLastNumberTrivia();

  Future cacheNumberTrivia(NumberTriviaModel triviaToCashe);
}

class NumberTriviaLocalDataSourceImpl implements NumberTriviaLocalDataSource {
  final SharedPreferences sharedPreferences;

  NumberTriviaLocalDataSourceImpl(this.sharedPreferences);
  @override
  Future<NumberTriviaModel> getLastNumberTrivia() async {
    final jsonString = sharedPreferences.getString(CASHED_NUMBER_TRIVIA);
    if (jsonString != null) {
      return Future.value(NumberTriviaModel.fromJson(json.decode(jsonString)));
    } else {
      throw CacheException();
    }
  }

  @override
  Future cacheNumberTrivia(NumberTriviaModel triviaToCashe) async{
    final jsonString = json.encode(triviaToCashe.toJson());
    sharedPreferences.setString(CASHED_NUMBER_TRIVIA, jsonString);
  }
}
