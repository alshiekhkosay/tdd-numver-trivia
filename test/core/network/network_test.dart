import 'package:flutter_test/flutter_test.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:number_trivia/core/network/network.dart';

import 'network_test.mocks.dart';

@GenerateNiceMocks([MockSpec<InternetConnectionChecker>()])
void main() {
  late MockInternetConnectionChecker mockInternetConnectionChecker;
  late NetworkInfoImp networkInfo;

  setUp(() {
    mockInternetConnectionChecker = MockInternetConnectionChecker();
    networkInfo = NetworkInfoImp(mockInternetConnectionChecker);
  });

  test('should forword the call to InternetConnection.hasConnection', () {
    //arrenge
    final tHasConnection = Future.value(true);

    when(mockInternetConnectionChecker.hasConnection)
        .thenAnswer((_) => tHasConnection);

    //act
    final result = networkInfo.isConnected;
    //assert
    verify(mockInternetConnectionChecker.hasConnection);
    expect(result, tHasConnection);
  });
}
