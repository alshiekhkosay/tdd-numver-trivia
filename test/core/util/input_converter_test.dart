import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:number_trivia/core/util/input_converter.dart';

void main() {
  late InputConverter inputConverter;

  setUp(() {
    inputConverter = InputConverter();
  });

  test('should return an int when the string is valid', () {
    //act
    final result = inputConverter.stringToUnSignedInteger('123');
    //assert
    expect(result, const Right(123));
  });

  test('should return failure when the string is not valid ', () {
    //act
    final result = inputConverter.stringToUnSignedInteger('abc');
    //assert
    expect(result, Left(InvalidInputFailure()));
  });
  test('should return failure when the number is negative ', () {
    //act
    final result = inputConverter.stringToUnSignedInteger('-52');
    //assert
    expect(result, Left(InvalidInputFailure()));
  });
}
