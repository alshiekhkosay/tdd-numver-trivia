import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:number_trivia/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:number_trivia/features/number_trivia/domain/entities/number_trivia.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  const tNumberTriviaModel = NumberTriviaModel(text: 'Test Text', number: 11);

  test(
    "should ",
    () async {
      //assert
      expect(tNumberTriviaModel, isA<NumberTrivia>());
    },
  );

  group('fromJSon', () {
    test(
      "should return a valid model from json that has an int value",
      () async {
        //arrenge
        final Map<String, dynamic> jsonMap =
            json.decode(fixture('trivia.json'));
        //act
        final result = NumberTriviaModel.fromJson(jsonMap); //assert
        expect(result, tNumberTriviaModel);
      },
    );
    test(
      "should return a valid model from json that has an double value",
      () async {
        //arrenge
        final Map<String, dynamic> jsonMap =
            json.decode(fixture('trivia_double.json'));
        //act
        final result = NumberTriviaModel.fromJson(jsonMap); //assert
        expect(result, tNumberTriviaModel);
      },
    );
  });

  group('toJson', () {
    test(
      "should ",
      () async {
        //act
        final Map<String, dynamic> jsonMap = tNumberTriviaModel.toJson();
        final expectedMap = {
          "text": "Test Text",
          "number": 11,
        };
        //assert
        expect(jsonMap, expectedMap);
      },
    );
  });
}
