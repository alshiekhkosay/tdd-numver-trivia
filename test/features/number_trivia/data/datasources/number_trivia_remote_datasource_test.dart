import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';
import 'package:number_trivia/core/errors/exception.dart';
import 'package:number_trivia/features/number_trivia/data/datasources/number_trivia_remote_data_source.dart';
import 'package:number_trivia/features/number_trivia/data/models/number_trivia_model.dart';

import '../../../../fixtures/fixture_reader.dart';
import 'number_trivia_remote_datasource_test.mocks.dart';

@GenerateNiceMocks([MockSpec<http.Client>()])
void main() {
  late NumberTriviaRemoteDataSourceImpl dataSource;
  late MockClient mockClient;
  setUp(() {
    mockClient = MockClient();
    dataSource = NumberTriviaRemoteDataSourceImpl(mockClient);
  });

  arrangeClientMockWith200() {
    when(mockClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (realInvocation) async => http.Response(fixture('trivia.json'), 200));
  }

  arrangeClientMockWith404() {
    when(mockClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (realInvocation) async => http.Response('Something Went Wrong', 404));
  }

  group('getConcreteNumberTrivia', () {
    const tNumber = 1;
    final tNumberTriviaModel =
        NumberTriviaModel.fromJson(json.decode(fixture('trivia.json')));
    test('''should performe get request with on URL with Number
        and with headers set to application/json''', () {
      //arrenge
      arrangeClientMockWith200();
      //act
      dataSource.getConcreteNumberTrivia(tNumber);
      //assert
      verify(mockClient.get(Uri.parse('http://numbersapi.com/$tNumber'),
          headers: {'Content-Type': 'application/json'}));
    });

    test('should return NumberTriviaModel when the response is 200', () async {
      //arrenge
      arrangeClientMockWith200();
      //act
      final result = await dataSource.getConcreteNumberTrivia(tNumber);
      //assert
      expect(result, equals(tNumberTriviaModel));
    });

    test('should throw ServerException when the response is not 200', () async {
      //arrenge
      arrangeClientMockWith404();
      //act
      final call = dataSource.getConcreteNumberTrivia;
      //assert
      expect(
          () => call(tNumber), throwsA(const TypeMatcher<ServerException>()));
    });
  });

  group('getRandomNumberTrivia', () {
    
    final tNumberTriviaModel =
        NumberTriviaModel.fromJson(json.decode(fixture('trivia.json')));
    test('''should performe get request with on URL with random
        and with headers set to application/json''', () {
      //arrenge
      arrangeClientMockWith200();
      //act
      dataSource.getRandomNumberTrivia();
      //assert
      verify(mockClient.get(Uri.parse('http://numbersapi.com/random'),
          headers: {'Content-Type': 'application/json'}));
    });

    test('should return NumberTriviaModel when the response is 200', () async {
      //arrenge
      arrangeClientMockWith200();
      //act
      final result = await dataSource.getRandomNumberTrivia();
      //assert
      expect(result, equals(tNumberTriviaModel));
    });

    test('should throw ServerException when the response is not 200', () async {
      //arrenge
      arrangeClientMockWith404();
      //act
      final call = dataSource.getRandomNumberTrivia;
      //assert
      expect(
          () => call(), throwsA(const TypeMatcher<ServerException>()));
    });
  });

}
