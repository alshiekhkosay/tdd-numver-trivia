import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:number_trivia/core/errors/exception.dart';
import 'package:number_trivia/features/number_trivia/data/datasources/number_trivia_local_data_source.dart';
import 'package:number_trivia/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../fixtures/fixture_reader.dart';
import 'number_trivia_local_datasource_test.mocks.dart';

@GenerateNiceMocks([MockSpec<SharedPreferences>()])
void main() {
  late MockSharedPreferences mockSharedPreferences;
  late NumberTriviaLocalDataSourceImpl dataSource;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    dataSource = NumberTriviaLocalDataSourceImpl(mockSharedPreferences);
  });

  group('get Number Trivia', () {
    final tNumberTrivia =
        NumberTriviaModel.fromJson(json.decode(fixture('trivia_local.json')));
    test('should return NumberTrivia from sharedpreferences when there is one',
        () async {
      //arrenge
      when(mockSharedPreferences.getString(any))
          .thenReturn(fixture('trivia_local.json'));
      //act
      final result = await dataSource.getLastNumberTrivia();
      //assert
      verify(mockSharedPreferences.getString(CASHED_NUMBER_TRIVIA));
      expect(result, tNumberTrivia);
    });
    test('should throw cache exception when there is not a cached value',
        () async {
      //arrenge
      when(mockSharedPreferences.getString(any)).thenReturn(null);
      //act
      final call = dataSource.getLastNumberTrivia;
      //assert
      // verify(mockSharedPreferences.getString('CASHED_NUMBER_TRIVIA'));

      expect(() => call(), throwsA(const TypeMatcher<CacheException>()));
    });
  });

  group('cahce Number Trivia', () {
    const tNumberTrivia = NumberTriviaModel(text: 'test', number: 1);
    test('should call SharedPreferences to cahce data', () {
      //act
      dataSource.cacheNumberTrivia(tNumberTrivia);
      //assert
      final expectedJsonString = json.encode(tNumberTrivia.toJson());
      verify(mockSharedPreferences.setString(
          CASHED_NUMBER_TRIVIA, expectedJsonString));
    });
  });
}
